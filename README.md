# README for obs-dl.sh
This script makes it easy to download an asset from the [Open Build Service](https://build.opensuse.org/) from cli.

## Upstream
This repository is the original: [https://gitlab.com/bgstack15/obs-dl](https://gitlab.com/bgstack15/obs-dl)

## Alternatives
Download the assets in a web browser and a login session.

## How to use
Set environment variables USERNAME and PASSWORD and then run command

    ./obs-dl https://build.opensuse.org/source/home:bgstack15/freefilesync/_service:recompress:tar_scm:FreeFileSync.tar.gz?rev=9410c094a8ce7fb9df4042c405ea0c7b

## Reason for existing
My build server is headless and I don't always want to use my desktop to download a large asset just to push it up to my dev system.

## Dependencies
Bourne shell

## References
man 1 curl
Original work.
