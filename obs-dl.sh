#!/bin/sh
# Startdate: 2020-11-30
# Works!

ua="Mozilla/5.0 (X11; Linux x86_64; rv:68.l9) Gecko/20100101" # not that important
cj=./.cj
file="${1}" ; test -z "${file}" && { echo "Need file to download! Aborted." 1>&2 ; exit 1 ; }
test -z "${OBS_PASSWORD}" && { echo "Need OBS_PASSWORD set. Aborted." 1>&2 ; exit 1 ; }
test -z "${OBS_USERNAME}" && { echo "Need OBS_USERNAME set. Aborted." 1>&2 ; exit 1 ; }

# Functions
url_var() {
   LC_ALL=C
   ___encoded=""
   ___i=""
   ___c=""
   ___temp="$( mktemp )"
   # needs GNU head
   echo "${1}" | sed -r -e 's/(.)/\1\n/g;' | head -n -1 | while read char ;
   do
      #echo "Char: ${char}"
      test '\0' != "${char}" &&
      case "${char}" in
         [a-zA-Z0-9/_.~-] ) ___encoded="${___encoded}${char}" ;;
         * ) ___encoded="${___encoded}$( $( which printf ) "%%%02x" "'${char}" )" ;;
      esac
      echo "${___encoded}" > "${___temp}"
   done
   ___encoded="$( cat "${___temp}" )" ; rm -f "${___temp}"
   echo "${___encoded}"
}

# html-encode the password, because of special characters...
OBS_PASSWORD="$( url_var "${OBS_PASSWORD}" )"

# visit page to get authenticity token and also the first part of the cookie, openSUSE_session
page="$( curl -s -b "${cj}" -c "${cj}" 'https://build.opensuse.org/' -H 'Host: build.opensuse.org' -H "User-Agent: ${ua}" -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Connection: keep-alive' )"
# must extract "csrf-token" value for "authenticity_token"
at="$( echo "${page}" | awk -F'"' '/csrf-token/{print $4}' | sed -r -e 's/\+/%2B/g;' -e 's/=/%3D/g;' )"

# authenticate to get the _obs_api_session part of the cookie
curl -s -b "${cj}" -c "${cj}" 'https://build.opensuse.org/ICSLogin/auth-up' -H 'Host: build.opensuse.org' -H "User-Agent: ${ua}" -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Referer: https://build.opensuse.org/' -H 'Connection: keep-alive' --data-raw "authenticity_token=${at}&context=default&proxypath=reserve&message=Please+log+in&username=${OBS_USERNAME}&password=${OBS_PASSWORD}&login=Log+In"

# and now download the real file
# it is possible that in the future I will need to adjust "Referer" to be directly related to the requested ${file}
curl -b "${cj}" -c "${cj}" "${file}" -J -O -R -H 'Host: build.opensuse.org' -H "User-Agent: ${ua}" -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Referer: https://build.opensuse.org/project/show/home:bgstack15' -H 'Connection: keep-alive'
